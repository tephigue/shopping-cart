CREATE TABLE `cart` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `color` enum("red", "green", "blue", "cyan", "magenta", "yellow", "black", "white") NOT NULL DEFAULT "white",
  PRIMARY KEY (`id`)
);

CREATE TABLE `cart_item` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `cart_id` int(11) UNSIGNED DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `volume` enum("liter", "kilogram", "package") NOT NULL DEFAULT "package",
  PRIMARY KEY (`id`),
  FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
);

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `password_real` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `user_cart` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
);