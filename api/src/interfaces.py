import graphene

from graphene import relay
from app import db_connection
from dataloaders.cart_item import CartItemCRUD
from common.enums import CartItemVolume

class ShoppingCartNode(graphene.relay.Node):
    class Meta:
        name = 'ShoppingCart'

    @staticmethod
    def to_global_id(type, id):
        return (id)

class CartItem(graphene.ObjectType):
    class Meta:
        interfaces = (ShoppingCartNode,)
        description = "Basic shopping cart item"

    name = graphene.String()
    volume = CartItemVolume()
    quantity = graphene.Int()

class Cart(graphene.ObjectType):
    class Meta:
        interfaces = (ShoppingCartNode,)
        description = "Basic shopping cart"

    cart_items = graphene.List(lambda: CartItem)
    name = graphene.String()
    date = graphene.Date()

    @classmethod
    def resolve_cart_items(cls, info, resolve_info):
        if info.get('id'):
            return [CartItem(**cart_item) for cart_item in CartItemCRUD.list_cart_items(db_connection, info['id'])]

        return []
