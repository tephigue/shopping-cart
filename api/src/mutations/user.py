import graphene
from app import db_connection

from dataloaders.user import UserCRUD

class CreateUser(graphene.Mutation):
    class Meta():
        interfaces = ()
        description = "Vytvoreni uzivatele"

    class Arguments:
        name = graphene.String()
        password = graphene.String()
        email = graphene.String()

    success = graphene.ID()

    def mutate(self, info, name=None, password=None, email=None):
        insert = {}

        if name:
            insert['name'] = name

        if password:
            insert['password'] = password
            insert['password_real'] = password
        if email:
            insert['email'] = email

        user_id = UserCRUD.insert(db_connection, insert)

        return CreateUser(success=True)
