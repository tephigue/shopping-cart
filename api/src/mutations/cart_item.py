import graphene
from app import db_connection

from dataloaders.cart import CartCRUD
from dataloaders.cart_item import CartItemCRUD

from common.enums import CartItemVolume


class AddCartItem(graphene.Mutation):
    class Meta():
        interfaces = ()
        description = "Pridavani veci do nakupniho kosiku"

    class Arguments:
        cart_id = graphene.ID()
        name = graphene.String()
        quantity = graphene.Int()
        volume = CartItemVolume()

    id = graphene.ID()

    def mutate(self, info, cart_id, name=None, quantity=None, volume=None):
        insert = {"cart_id": int(cart_id)}

        # TODO: chytreji :D
        if name:
            insert['name'] = name

        if quantity:
            insert['quantity'] = quantity

        if volume:
            insert['volume'] = volume
        else:
            insert['volume'] = "package"

        cart_item_id = CartItemCRUD.insert(db_connection, insert)

        return AddCartItem(id=cart_item_id)

class CheckCartItem(graphene.Mutation):
    class Meta():
        interfaces = ()
        description = "Pridavani veci do nakupniho kosiku"

    class Arguments:
        cart_item_id = graphene.ID()
        new_state = graphene.Boolean()

    success = graphene.Boolean()

    def mutate(self, info, cart_item_id, new_state):
        CartItemCRUD.update(db_connection, cart_item_id, {'checked': bool(new_state)})

        return CheckCartItem(success=True)