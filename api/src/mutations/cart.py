import graphene
from app import db_connection

from dataloaders.cart import CartCRUD
from dataloaders.cart_item import CartItemCRUD
from common.user import User


from common.enums import CartItemVolume

class AddCart(graphene.Mutation):
    class Meta():
        interfaces = ()
        description = "Vytvoreni kosiku"

    class Arguments:
        name = graphene.String()
        date = graphene.String()

    id = graphene.ID()

    def mutate(self, info, name=None, date=None):
        insert = {}

        # TODO: chytreji :D
        if name:
            insert['name'] = name

        if date:
            insert['date'] = date

        user_id = User.get_user_id()

        cart_id = CartCRUD.insert(db_connection, user_id, insert)

        return AddCart(id=cart_id)

class UpdateCart(graphene.Mutation):

    class Meta():
        interfaces = ()
        description = "Uprava kosiku"

    class Arguments:
        id = graphene.ID()
        name = graphene.String()
        date = graphene.String()

    success = graphene.Boolean()

    def mutate(self, info, id, name=None, date=None):
        update = {}

        # TODO: chytreji :D
        if name:
            update['name'] = name

        if date:
            update['date'] = date

        CartCRUD.update(db_connection, id, update)

        return UpdateCart(success=True)
