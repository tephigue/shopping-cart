import graphene

class CartItemVolume(graphene.Enum):
    liter = "liter"
    package = "package"
    kilogram = "kilogram"
