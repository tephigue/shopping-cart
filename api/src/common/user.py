from passlib.context import CryptContext

import os
import jwt
import time

JWT_SECRET = os.environ.get("JWT_SECRET", "5LIp3rT4jnYh3sl0")

class User:

    @staticmethod
    def get_context():
        pwd_context = CryptContext(
            schemes=["pbkdf2_sha256"],
            default="pbkdf2_sha256",
            pbkdf2_sha256__default_rounds=30000
        )
        return pwd_context

    @staticmethod
    def get_user_id():
        return 1

    @classmethod
    def check_password(cls, password, password_hash):
        return cls.get_context().verify(password, password_hash)

    @classmethod
    def get_hash(cls, password):
        return cls.get_context().encrypt(password)

def generate_jwt(name, user_id):
    # 60 * 60 * 24 * 3 = 259200
    valid = int(time.time()) + 259200
    encoded_jwt = jwt.encode(
        {'name': name,
         'id': user_id,
         'valid': valid},
        JWT_SECRET, algorithm='HS256'
    )

    return encoded_jwt

def decode_jwt(token):
    return jwt.decode(token, JWT_SECRET, algorithms=['HS256'])