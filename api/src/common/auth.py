from functools import wraps
from flask import request, g, abort
from common.user import decode_jwt, generate_jwt
from time import time
import os

def set_auth_token(response):
    if hasattr(g, 'auth_token'):
        response.headers['authorization'] = g.auth_token

    return response

def require_api_key(f):
    @wraps(f)

    def check_api_key(*args, **kwargs):
        g.auth_token = ""
        if not hasattr(g, 'auth_attempted') or not g.auth_attempted:  # Aby pripadny vnoreny dekorator nedelal nic

            g.logged = False
            g.auth_attempted = True  # neoverovat pro vnoreny dekorator a zakazat cache pro autentizovane
            g.user_id = None
            try:
                if os.environ.get('DEBUG', None):
                    g.user_id = 1
                    g.user_name = "nanuk"
                    g.logged = True
                    g.auth_token = ''

                else:
                    token = decode_jwt(request.headers.get('authorization'))
                    if token['valid'] > int(time()):
                        g.user_id = token['id']
                        g.user_name = token['name']
                        g.logged = True
                        g.auth_token = generate_jwt(token['name'], token['id'])

            except Exception as e:
                print("Checking user error: %s %s" % (type(e), e))

        if not g.logged:
            raise abort(403)

        return f(*args, **kwargs)

    return check_api_key