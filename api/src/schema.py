import graphene

from app import db_connection
from interfaces import CartItem, Cart

from common.user import User
from dataloaders.cart import CartCRUD

from mutations.cart import AddCart, UpdateCart
from mutations.cart_item import AddCartItem, CheckCartItem
from mutations.user import CreateUser
from common.auth import require_api_key

class ShoppingListAPIQuery(graphene.ObjectType):
    cart = graphene.Field(Cart, id=graphene.ID())
    carts = graphene.List(Cart)

    @require_api_key
    def resolve_cart(info, resolve_info, id=None):
        return CartCRUD.get_by_id(db_connection, id)

    @require_api_key
    def resolve_carts(info, resolve_info):
        return CartCRUD.list_by(db_connection, User.get_user_id())

class ShoppingListAPIMutations(graphene.ObjectType):
    add_cart = AddCart.Field()
    add_cart_item = AddCartItem.Field()
    update_cart = UpdateCart.Field()
    create_user = CreateUser.Field()
    check_cart_item = CheckCartItem.Field()

def return_schema():
    return graphene.Schema(query=ShoppingListAPIQuery, mutation=ShoppingListAPIMutations)
