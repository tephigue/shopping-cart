from dataloaders.crud import CRUD
from common.user import User

class UserCRUD(CRUD):
    table = "user"

    @classmethod
    def insert(cls, db_connection, insert):
        insert['password'] = User.get_hash(insert['password'])
        return super().insert(db_connection, insert)
