from dataloaders.crud import CRUD

class CartItemCRUD(CRUD):
    table = "cart_item"

    @classmethod
    def list_cart_items(cls, db_connection, cart_id):
        sql = """
            SELECT id, name, volume, quantity
            FROM cart_item 
            WHERE cart_id = %s
        """

        db_connection.ping(reconnect=True)
        with db_connection.cursor() as cursor:
            # Read a single record
            cursor.execute(sql, (cart_id,))
            result = cursor.fetchall()

        return result

