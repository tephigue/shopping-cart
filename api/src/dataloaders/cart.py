from dataloaders.crud import CRUD
from dataloaders.user_cart import UserCartCRUD

class CartCRUD(CRUD):
    table = "cart"

    @classmethod
    def insert(cls, db_connection, user_id, insert):
        cart_id = super().insert(db_connection, insert)
        lastrowid = UserCartCRUD.insert(db_connection, {"cart_id": cart_id, "user_id": user_id})

        return lastrowid

    @classmethod
    def list_by(cls, db_connection, user_id):
        sql = """
            SELECT c.id, c.name
            FROM `cart` c 
            INNER JOIN `user_cart` uc ON uc.cart_id = c.id
            WHERE uc.user_id = %s
        """
        db_connection.ping(reconnect=True)
        with db_connection.cursor() as cursor:
            cursor.execute(sql, (user_id,))
            result = cursor.fetchall()

        return result