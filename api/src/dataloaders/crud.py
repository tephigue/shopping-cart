
class CRUD:
    table = None

    @classmethod
    def get_by_id(cls, db_connection, id):
        # TODO: bacha na hvezdicku
        # - kdyz predas neco graphenu, co nema ve schema,
        # tak se z toho zvencne
        sql = """
            SELECT *
            FROM {table} WHERE id = %s
        """.format(table=cls.table)

        db_connection.ping(reconnect=True)
        with db_connection.cursor() as cursor:
            cursor.execute(sql, (id,))
            result = cursor.fetchone()

        return result

    @classmethod
    def get_by(cls, db_connection, row, value):
        # TODO: bacha na hvezdicku
        # - kdyz predas neco graphenu, co nema ve schema,
        # tak se z toho zvencne
        sql = """
                SELECT *
                FROM {table} WHERE {row} = %s
            """.format(table=cls.table, row=row)

        db_connection.ping(reconnect=True)
        with db_connection.cursor() as cursor:
            cursor.execute(sql, (value,))
            result = cursor.fetchone()

        return result

    @classmethod
    def list_by(cls, db_connection, row, value):
        sql = """
            SELECT * 
            FROM {table}
            WHERE `{row}` = %s
        """.format(table=cls.table, row=row)

        db_connection.ping(reconnect=True)
        with db_connection.cursor() as cursor:
            cursor.execute(sql, (value,))
            result = cursor.fetchall()

        return result

    @classmethod
    def insert(cls, db_connection, insert):
        sql = """
            INSERT INTO {table}
            ({keys})
            VALUES ({placeholder})
        """.format(table=cls.table,
                   keys=','.join(str(x) for x in insert.keys()),
                   placeholder=','.join('%s' for _ in range(len(insert))))

        try:
            db_connection.ping(reconnect=True)
            with db_connection.cursor() as cursor:
                print(list(insert.values()))
                cursor.execute(sql, list(insert.values()))

        except Exception as e:
            print("%s: %s" % (type(e), e))
        finally:
            return cursor.lastrowid

    @classmethod
    def update(cls, db_connection, id, update):
        sql = """
            UPDATE `{table}`
            SET {placeholder}
            WHERE `id` = %s
        """.format(table=cls.table,
                   placeholder=','.join('`{key}` = %s'.format(key=key) for key in update.keys()))

        try:
            db_connection.ping(reconnect=True)
            with db_connection.cursor() as cursor:
                update_list = list(update.values())
                update_list.append(id)
                cursor.execute(sql, update_list)

        except Exception as e:
            print("%s: %s" % (type(e), e))
        finally:
            return cursor.lastrowid
