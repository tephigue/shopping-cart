import os
import pymysql

def create_mysql_connection():
    return pymysql.connect(host=os.environ.get('MYSQL_MASTER_HOST', 'localhost'),
                           password=os.environ['MYSQL_MASTER_PASSWORD'],
                           port=int(os.environ.get('MYSQL_MASTER_PORT', 3306)),
                           user=os.environ['MYSQL_MASTER_USER'],
                           database=os.environ.get('MYSQL_MASTER_DATABASE', 'shoppingcart'),
                           connect_timeout=int(os.environ.get('MYSQL_MASTER_CONNECT_TIMEOUT', 1)),
                           charset=os.environ.get('MYSQL_MASTER_ENCODING', 'utf-8'),
                           cursorclass=pymysql.cursors.DictCursor,
                           autocommit=True)
