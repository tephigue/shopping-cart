import os
import uuid
from flask import Flask, request, g
from flask_graphql import GraphQLView
from config import create_mysql_connection

db_connection = create_mysql_connection()

def create_app():
    global app, db_connection

    import schema
    port = os.environ.get("UWSGI_HTTP_SOCKET", 8888)

    schema = schema.return_schema()
    app = Flask(__name__)#, template_folder="template_folder")
    app.add_url_rule('/v1/graphiql', view_func=GraphQLView.as_view('graphiql', schema=schema, graphiql=True))
    app.add_url_rule('/v1/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=False))

    def log_request():
        query = str(request.data)
        extra = {"ip": request.headers.get('X-Real-Ip', ''),
                                           "handler": str(request.path),
                                           "user_agent": str(request.user_agent)}
        if query:
            extra['graphql_query'] = query

        print("Processing request, extra: %s" % extra)

    app.before_request(log_request)

    def request_unique_id(response):
        uid = str(uuid.uuid4())
        response.headers['X-UUID'] = uid
        return response

    app.after_request(request_unique_id)

    print('Starting GraphQL server on %s' % port)
    print()
    print('  GraphiQL:              http://localhost:%s/graphiql' % port)
    print('  Queries and Mutations: http://localhost:%s/graphql' % port)
    print()
    print('Database:')
    print('   master: {} ({}@{}:{})'.format(os.environ.get('MYSQL_MASTER_DATABASE'), os.environ.get('MYSQL_MASTER_USER'), os.environ.get('MYSQL_MASTER_HOST'), os.environ.get('MYSQL_MASTER_PORT')))
    print()

    from handlers.user import login, logout
    app.add_url_rule('/v1/login', view_func=login, methods=['GET', 'POST'])
    app.add_url_rule('/v1/logout', view_func=logout, methods=['GET', 'POST'])

    from common.auth import set_auth_token
    app.after_request(set_auth_token)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0', port=8888, use_reloader=True, debug=True)