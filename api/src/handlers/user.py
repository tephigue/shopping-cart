from app import db_connection

from flask import make_response, request, Response, abort, g
from common.user import User
from dataloaders.user import UserCRUD
from common.user import generate_jwt, decode_jwt

def login():
    name = request.headers.get('x-name')
    password = request.headers.get('x-password')
    user = UserCRUD.get_by(db_connection, 'name', name)

    if not user:
        abort(401)

    verified = User.check_password(password, user['password'])

    if not verified:
        abort(401)

    g.auth_token = generate_jwt(name, user['id'])

    return make_response(Response(status=200))


def logout():
    response = Response()
    g.auth_token = generate_jwt("", 0) #(name, user['id'])
    return make_response(response)
